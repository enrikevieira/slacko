# Slacko #

Baixar o arquivo devx_slacko_5.6.0.sfs, que permite compilar aplicacoes.
- http://distro.ibiblio.org/puppylinux/puppy-5.6/devx_slacko_5.6.0.sfs

Atualizar as aplicacoes instaladas e o repositorio atraves do Puppy Package Manager

Copiar e colar a configuracao do URXVT

!Rxvt*keysym.0xFF50:\001
!Rxvt*keysym.0xFF57:\005
! but urxvt in quirky etc needs these...
!Rxvt.keysym.Home: \033[1~
!Rxvt.keysym.End: \033[4~
! broomdodger reports this works in both 431 and quirky (rxvt and urxvt)...
Rxvt.keysym.0xFF50: \033[1~
Rxvt.keysym.0xFF57: \033[4~ 

!Rxvt*menu: /root/.rxvt.menu
Rxvt*saveLines: 1000

urxvt.foreground:#f8f8f8f8f8f8
urxvt.background:#000000000000
urxvt.underlineColor:#ffff00000000

! fonts
! run "fc-list" for a list of available fonts
urxvt.font: xft:Nimbus Mono L:style=Regular:pixelsize=16

urxvt.geometry:80x25
urxvt.scrollBar: True
urxvt.scrollTtyOutput: False
urxvt.scrollTtyKeypress: True
urxvt.secondaryScroll: True
urxvt.saveLines: 1000
urxvt.cursorUnderline:True
urxvt.cursorBlink:True

urxvt.transparent:On
urxvt.shading:85
urxvt.borderLess:Off


Instalar as seguintes aplicacoes
- GIT
- gcc-gfortran
- gcc-java

Baixar o instalador do R no www.r-project.org
Baixar o editor sublime no http://www.sublimetext.com/

Criar links simbolicos em /usr/bin para os itens baixados



